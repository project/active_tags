# Active Tags

Enhance your entity reference fields with an intuitive tags input widget.
This widget seamlessly integrates with the Drupal Entity Autocomplete,
providing a user-friendly tagging experience and optimizing performance
for smoother and more efficient interactions.

* For a full description of the module, visit the
  [project page](https://www.drupal.org/project/active_tags).
* Submit bug reports and feature suggestions, or track changes in the
  [issue queue](https://www.drupal.org/project/issues/active_tags).

## Table of contents
- [Requirements](#requirements)
- [Installation](#installation)
- [Usage](#usage)
- [Configuration](#configuration)
- [Developers Guide](#developers-guide)
- [Maintainers](#maintainers)

## Requirements

This module requires no modules or libraries outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module.
For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Usage

To use the Active Tags widget for tagging input, set the reference
or selection field widget to Active Tags.

### Managing Form Display

1. **Navigate to Content Types:**
  - Go to Administration > Structure > Content types.
  - Select the content type you wish to edit and click on "Manage fields".

2. **Access Form Display Settings:**
  - Go to the "Manage form display" tab of the entity type.

3. **Change Widget Type:**
  - From the drop-down menu, change the Widget Type to either
    `"Autocomplete (Active Tags)"` or `"Select (Active Tags)"`.

4. **Save Changes:**
  - Click Save to apply the new widget settings.

By following these steps, the Active Tags widget will be enabled for
the selected field, enhancing the tagging input interface.

This allows editors to efficiently enter existing tags
and create new ones when adding new content.

## Configuration

### Widget Settings: Active Tags

#### Autocomplete Matching:
- **Starts with**: Suggests results that start with the entered characters.
- **Contains**: Suggests results that contain the entered characters.
    Note: This can impact performance on sites with a large number of entities.

#### Number of Results:
Specify the number of suggestions to display. Use 0 for unlimited results.

#### Minimum Length:
Set the minimum number of characters needed to trigger the suggestion list.

#### Delimiter:
Define a character to separate terms, in addition to the Enter key.

#### Tag Style:
- **Rectangular**: Displays tags in a rectangular shape.
- **Bubble Style**: Displays tags in a bubble shape.

#### Include Entity ID:
Enable this option to include the entity ID within each tag.

#### Use Multi-Value Design:
If enabled, the widget will always use the design for multiple values,
even for fields that typically only allow a single value.

#### Not Exist Message:
Message to display when the term doesn't exist but can be created.

#### Not Found Message:
Message to display when the term doesn't exist and can't be created.

#### Placeholder:
Enter the text to display inside the field before a value is entered,
typically a sample value or a brief description of the expected format.

#### Include Info Label:
Enable this to show an additional label with information next to
the entity label.

#### Info Label Content:
The content to display in the additional label. Use tokens for
dynamic values.

By configuring these settings, you can tailor the Active Tags widget to
meet your specific needs, enhancing the user experience with a modern,
efficient, and flexible tagging system. This allows for better data management
and a more intuitive interface for content editors.

### Future Enhancements

Stay tuned for future updates and enhancements to the Active Tags module,
ensuring continued improvements and additional features to meet evolving needs.

## Developers Guide

This section provides guidance for developers on how to utilize
the Active Tags widget in their Drupal modules using the Form API.
The examples below demonstrate how to configure and implement
the Active Tags widget for both autocomplete and select elements.

### Autocomplete Example

To add an autocomplete field with Active Tags to your form,
use the following configuration:

```php
$form['my_element'] = [
  '#type' => 'entity_autocomplete_active_tags',
  '#title' => $this->t('Active tags an autocomplete element'),
  '#target_type' => 'node',
  '#tags' => TRUE,
  '#default_value' => $entities,
  '#selection_handler' => 'default',
  '#selection_settings' => [
    'target_bundles' => ['article', 'page'],
    'info_label' => [node:type],
  ],
  '#autocreate' => [
    'bundle' => 'article',
    'uid' => a valid user ID,
  ],
  '#match_limit' => 10,
  '#min_length' => 0,
  '#delimiter' => '',
  '#style' => 'rectangle',
  '#show_entity_id' => FALSE,
  '#not_exist_message' => $this->t('@not_exist_message', [
    '@not_exist_message' =>
      "No matches found, Press Enter to add <strong>'@term'</strong>."
  ]),
  '#not_found_message' => $this->t('@not_found_message', [
    '@not_found_message' =>
      "No matching suggestions found for <strong>'@term'</strong>."
  ]),
  '#placeholder' => $this->t('@placeholder', ['@placeholder' => "Enter..."]),
];
```

### Select example:

To add a select field with Active Tags to your form,
use the following configuration:

```php
$form['example_active_tags_select'] = [
  '#type' => 'select_active_tags',
  '#mode' => 'select',
  '#title' => $this->t('Active tags a select element'),
  '#options' => [
    '1' => $this->t('One'),
    '2' => $this->t('Two'),
    '3' => $this->t('Three'),
  ],
  '#match_limit' => 10,
  '#style' => 'rectangle',
  '#placeholder' => $this->t('@placeholder', ['@placeholder' => "Select..."]),
];
```

These examples demonstrate how to integrate the Active Tags widget into
your Drupal forms using the Form API. By leveraging these configurations,
developers can enhance their forms with modern tagging capabilities,
providing a better user experience for content editors.

## Maintainers

- Mahyar SBT - [mahyarsbt](https://www.drupal.org/u/mahyarsbt)
