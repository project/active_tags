<?php

/**
 * @file
 * The Active Tags module improves autocomplete fields using JQuery UI.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function active_tags_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.active_tags':
      $text = file_get_contents(dirname(__FILE__) . "/README.md");
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        $output = '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        $output = $filter->process($text, 'en');
      }

      return $output;
  }
}

/**
 * Determines whether the active theme is Gin, or a sub-theme of Gin.
 */
function _active_tags_is_gin_theme_active(): bool {
  $active_theme = Drupal::getContainer()->get('theme.manager')->getActiveTheme();
  $base_themes = (array) $active_theme->getBaseThemeExtensions();
  return $active_theme->getName() === 'gin' || array_key_exists('gin', $base_themes);
}

/**
 * Determines whether the active theme is Claro, or a sub-theme of Claro.
 */
function _active_tags_is_claro_theme_active(): bool {
  $active_theme = Drupal::getContainer()->get('theme.manager')->getActiveTheme();
  $base_themes = (array) $active_theme->getBaseThemeExtensions();
  // If the active theme is Gin, return false.
  if ($active_theme->getName() === 'gin' || array_key_exists('gin', $base_themes)) {
    return FALSE;
  }

  return $active_theme->getName() === 'claro' ||  array_key_exists('claro', $base_themes);
}

/**
 * Determines whether the active theme is Claro, or a sub-theme of Claro.
 */
function _active_tags_is_seven_theme_active(): bool {
  $active_theme = Drupal::getContainer()->get('theme.manager')->getActiveTheme();
  $base_themes = (array) $active_theme->getBaseThemeExtensions();
  return $active_theme->getName() === 'seven' ||  array_key_exists('seven', $base_themes);
}
