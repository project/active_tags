/**
 * @file
 * Behaviors for initializing the Active Tags Autocomplete widget.
 */

(function ($, Drupal, drupalSettings, activeTags, debounce, JSON, Sortable, once) {

  /**
   * Initializes the Active Tags autocomplete widget for the input field.
   *
   * @param {Element} input
   *   The input element for the Active Tags widget.
   */
  function initAutocomplete(input) {
    // Set the context for the input.
    this.input = input;

    // Retrieve settings specific to the current autocomplete instance.
    const autocompleteSettings = drupalSettings.activeTagsAutocomplete[$(input).attr("id")];

    // Set local variables for identifier and cardinality.
    //const identifier = input.name || input.dataset.identifier;
    const identifier = input.dataset.identifier || input.name;
    const cardinality = parseInt(input.dataset.cardinality, 10) || Infinity;

    // Parse showAvatar as an integer from the dataset.
    const showAvatar = parseInt(input.dataset.showAvatar, 10) || 0;

    // Check the input has specific classes for auto-create and bubble styles.
    const autoCreate = $(input).hasClass('active-tags--auto-create');
    const bubbleStyle = $(input).hasClass('active-tags__tag__bubble');

    // Define the template for no match dropdown items.
    const dropdownItemNoMatch = function (data) {
      // Determine the appropriate message for no match scenarios.
      let t_args = { "@term": data.value };
      const noMatchMessage = autoCreate
        ? (autocompleteSettings.not_exist_message
          ? Drupal.formatString(autocompleteSettings.not_exist_message, t_args)
          : Drupal.t('No matches found, Press Enter to add <strong>@term</strong>.', t_args))
        : (autocompleteSettings.not_found_message
          ? Drupal.formatString(autocompleteSettings.not_found_message, t_args)
          : Drupal.t('No matching suggestions found for <strong>@term</strong>.', t_args));

      return !isTagLimitReached(cardinality, identifier)
        ? `<div class="${activeTags.settings.classNames.dropdownItem} active-tags__dropdown__item--no-match"
            value="noMatch"
            tabindex="0"
            role="option">
          <p>${noMatchMessage}</p>
       </div>`
        : '';
    };

    // Initialize the Active Tags widget.
    const activeTags = Drupal.activeTags(input, {
      dropdown: {
        enabled: parseInt(input.dataset.suggestionsDropdown, 10),
        highlightFirst: true,
        fuzzySearch: !!parseInt(input.dataset.matchOperator, 10),
        maxItems: parseInt(input.dataset.maxItems, 10) || Infinity,
        closeOnSelect: true,
        searchKeys: ['label'],
        mapValueTo: 'label',
      },
      templates: {
        tag: function(tagData) {
          return tagTemplate.call(this, tagData, input);
        },
        dropdownItem: function(tagData) {
          return suggestionItemTemplate.call(this, tagData, cardinality, identifier, showAvatar);
        },
        dropdownFooter: function(tagData) {
          return suggestionFooterTemplate.call(this, tagData, cardinality, identifier);
        },
        dropdownItemNoMatch: dropdownItemNoMatch,
      },
      classNames: {
        dropdown: bubbleStyle ? 'active-tags__tag__bubble active-tags__dropdown' : 'active-tags__dropdown',
      },
      whitelist: [],
      placeholder: parseInt(input.dataset.placeholder, 10),
      tagTextProp: 'label',
      editTags: false,
      maxTags: cardinality > 0 ? cardinality : Infinity,
    });

    this.activeTags = activeTags;

    /**
     * Binds Sortable to Active Tags main element
     * and specifies draggable items.
     */
    Sortable.create(activeTags.DOM.scope, {
      draggable: `.${activeTags.settings.classNames.tag}:not(active-tags__input)`,
      forceFallback: true,
      onEnd: () => {
        // Update Active Tags value according to the re-ordered nodes.
        document.onselectstart = null;
        activeTags.updateValueByDOMTags();
      },
    });

    // Active tags input event.
    const onInput = Drupal.debounce((e) => {
      const { value } = e.detail;
      handleAutocomplete.call(this, value, activeTags, activeTags.value.map((item) => item.entity_id), input, identifier, cardinality);
    }, 300);

    // Active tags change event.
    const onChange = Drupal.debounce(() => {
      // Configure settings for Active Tags.
      activeTags.settings.enforceWhitelist =
        isTagLimitReached(cardinality, identifier) && cardinality > 1
          ? false
          : !autoCreate;
      activeTags.settings.skipInvalid = isTagLimitReached(cardinality, identifier)
        ? false
        : autoCreate;
    }, 300);

    // Input event (when a tag is being typed/edited.
    // e.detail exposes value, inputElm & isValid).
    activeTags.on('input', onInput);
    // Change event (any change to the value has occurred.
    // e.detail.value callback listener argument is a String).
    activeTags.on('change', onChange);

    // Trigger the onChange event to apply initial settings.
    onChange();

    // Handle dropdown suggestions enabled scenario.
    if (!activeTags.settings.dropdown.enabled) {
      const tagsElement = document.querySelector(`.${identifier}`);
      if (tagsElement) {
        tagsElement.classList.add('active-tags-select');
        tagsElement.addEventListener('click', (e) => {
          if (e.target.classList.contains('active-tags__input') && e.target.closest(`.${identifier}`)) {
            handleClickEvent.call(this, e, activeTags, input, identifier, cardinality);
          }
        });
      } else {
        console.warn(`Element with identifier ${identifier} not found.`);
      }
    }

    // Add an event listener to handle image loading errors.
    document.addEventListener('DOMContentLoaded', () => {
      const images = document.querySelectorAll('.active-tags__info-label-img');
      images.forEach(img => {
        img.addEventListener('error', () => {
          img.style.visibility = 'hidden';
        });
      });
    });
  }

  /**
   * Initialize the Active Tags widget for the select element.
   *
   * @param {Element} select
   *   The select element for the Active Tags widget.
   */
  function initSelect(select) {
    // Set the select element to the instance's select property.
    this.select = select;
    this.mode = select.dataset.mode;

    // Retrieve settings specific to the current autocomplete instance.
    const selectSettings = drupalSettings.activeTagsSelect[$(select).attr("id")];

    // Assign the identifier from the dataset of the select element
    // to the instance's identifier property.
    const identifier = select.dataset.identifier;

    // Parse the cardinality value from the dataset, or default
    // to Infinity if not present.
    const cardinality = parseInt(select.dataset.cardinality, 10) || Infinity;

    // Destructure additional dataset properties for matchOperator,
    // matchLimit, and placeholder.
    const { matchOperator, matchLimit, placeholder } = select.dataset;

    // Check if the select element has the auto-create class.
    const autoCreate = $(select).hasClass('active-tags--auto-create');

    // Define the template for no match dropdown items.
    const dropdownItemNoMatch = function (data) {
      // Determine the appropriate message for no match scenarios.
      let t_args = { "@term": data.value };
      const noMatchMessage = autoCreate
        ? (selectSettings.not_exist_message
          ? Drupal.formatString(selectSettings.not_exist_message, t_args)
          : Drupal.t('No matches found, Press Enter to add <strong>@term</strong>.', t_args))
        : (selectSettings.not_found_message
          ? Drupal.formatString(selectSettings.not_found_message, t_args)
          : Drupal.t('No matching suggestions found for <strong>@term</strong>.', t_args));

      return !isTagLimitReached(cardinality, identifier)
        ? `<div class="${this.activeTagsSelect.settings.classNames.dropdownItem} active-tags__dropdown__item--no-match"
              value="noMatch"
              tabindex="0"
              role="option">
            <p>${noMatchMessage}</p>
         </div>`
        : '';
    };

    // Initialize arrays to store options and selected options.
    const options = [];
    const selected = [];

    // Iterate over each option element in the select element.
    [...select.options].forEach((option) => {
      // Add each option to the options array with its value and text.
      options.push({ value: option.value, text: option.text });

      // If the option is selected, add it to the selected array.
      if (option.selected) {
        selected.push({ value: option.value, text: option.text });
      }
    });

    // Insert an input element to attach Active Tags.
    const input = document.createElement('input');
    input.setAttribute('class', select.getAttribute('class'));
    input.value = JSON.stringify(selected);
    select.before(input);

    // Initialize the Active Tags widget with the provided configuration.
    this.activeTagsSelect = Drupal.activeTags(input, {
      dropdown: {
        enabled: 0,
        fuzzySearch: !!parseInt(matchOperator, 10),
        maxItems: matchLimit || Infinity,
        highlightFirst: true,
        searchKeys: ['text'],
        mapValueTo: 'text',
      },
      templates: {
        tag: tagSelectTemplate,
        dropdownItem: function(tagData) {
          return dropdownItemTemplate.call(this, tagData, cardinality, identifier);
        },
        dropdownFooter: function(tagData) {
          return suggestionFooterSelectTemplate.call(this, tagData, cardinality, identifier);
        },
        dropdownItemNoMatch: dropdownItemNoMatch,
      },
      classNames: {
        input: 'active-tags__input-select',
      },
      whitelist: options,
      enforceWhitelist: true,
      editTags: !!this.mode,
      maxTags: cardinality > 0 ? cardinality : Infinity,
      tagTextProp: 'text',
      placeholder,
    });

    // Remove active-tags--select class to keep Active Tags styles.
    if (this.mode) {
      const tagsElement = document.querySelector(`.${identifier}`);
      tagsElement.classList.remove('active-tags--select');
    }

    // Bind Sortable to Active Tags main element.
    Sortable.create(this.activeTagsSelect.DOM.scope, {
      draggable: `.${this.activeTagsSelect.settings.classNames.tag}:not(active-tags__input-select)`,
      forceFallback: true,
      onEnd: () => {
        Array.from(
          this.activeTagsSelect.DOM.scope.querySelectorAll(
            `.${this.activeTagsSelect.settings.classNames.tag}`
          )
        ).forEach((tag) => {
          const value = tag.getAttribute('value');
          const option = select.querySelector(`option[value="${value}"]`);
          if (option) {
            select.removeChild(option);
            select.appendChild(option);
            option.selected = tag.classList.contains('active-tags__tag');
          }
        });
      },
    });

    // Listen to add tag event.
    this.activeTagsSelect.on('add', (e) => {
      const { value } = e.detail.data;
      const option = select.querySelector(`option[value="${value}"]`);
      if (option) {
        select.removeChild(option);
        select.appendChild(option);
        option.selected = true;
      }
    });

    // Listen to remove tag event.
    this.activeTagsSelect.on('remove', (e) => {
      const { value } = e.detail.data;
      const option = select.querySelector(`option[value="${value}"]`);
      if (option) {
        option.selected = false;
      }
    });
  }

  /**
   * Initialize the Active Tags widget for the facets element.
   *
   * @param {Element} select
   *   The facets element for the Active Tags widget.
   */
  function initFacets(facet) {
    // Set the select element to the instance's select property.
    this.facet = facet;

    const $widgetLinks = $(this.facet).find('.facet-item > a');
    const $whitelist = [];
    const $selected = [];

    // Eslint-disable-next-line func-names.
    $widgetLinks.each(function (key, link) {
      const $link = $(link);
      const value = link.querySelector('.facet-item__value').textContent;
      const count = link.querySelector('.facet-item__count').textContent;
      const entityId = link.dataset.drupalFacetItemValue;

      // Create whitelist for Active tags suggestions
      // with values coming from links.
      $whitelist.push({
        value: $link.attr('href'),
        text: value,
        count: count.trim(),
        entity_id: entityId,
      });
      // If link is active, add to the input
      // (which will be used on Active tags).
      if ($link.hasClass('is-active')) {
        $selected.push({
          value: $link.attr('href'),
          text: value,
          count: count.trim(),
          entity_id: entityId,
        });
      }
    });

    // Check if an input element with the specified class exists.
    let input = document.querySelector('input.js-facets-active-tags');
    if (!input) {
      input = document.createElement('input');
      input.setAttribute('class', 'active-tags__input-facets');
      facet.before(input);
    }
    input.value = JSON.stringify($selected);
    facet.before(input);
    input.dataset.showEntityId = drupalSettings.activeTags.facets_widget.show_entity_id;

    // eslint-disable-next-line no-undef
    this.activeTagsFacet = Drupal.activeTags(input, {
      dropdown: {
        enabled: 0,
        highlightFirst: true,
        searchKeys: ['text'],
        fuzzySearch: !!parseInt(
          drupalSettings.activeTags.facets_widget.match_operator,
          10,
        ),
        maxItems:
          drupalSettings.activeTags.facets_widget.max_items ?? Infinity,
      },
      templates: {
        tag: function(tagData) {
          return tagFacetsTemplate.call(this, tagData, input);
        },
        dropdownItem: suggestionFacetsItemTemplate,
        dropdownFooter() {
          return '';
        },
      },
      whitelist: $whitelist,
      enforceWhitelist: true,
      editTags: false,
      placeholder: drupalSettings.activeTags.facets_widget.placeholder,
    });

    /**
     * Binds Sortable to Active tags main element and specifies draggable items.
     */
    Sortable.create(this.activeTagsFacet.DOM.scope, {
      draggable: `.${this.activeTagsFacet.settings.classNames.tag}:not(active-tags__input-facets)`,
      forceFallback: true,
      onEnd() {
        this.activeTagsFacet.updateValueByDOMTags();
      },
    });

    /**
     * Listens to add tag event and updates facets values accordingly.
     */

    // eslint-disable-next-line func-names
    this.activeTagsFacet.on('add', function (e) {
      const { value } = e.detail.data;
      e.preventDefault();
      $(facet).trigger('facets_filter', [value]);
    });

    /**
     * Listens to remove tag event and updates facets values accordingly.
     */

    // eslint-disable-next-line func-names
    this.activeTagsFacet.on('remove', function (e) {
      const { value } = e.detail.data;
      e.preventDefault();
      $(facet).trigger('facets_filter', [value]);
    });
  }

  /**
   * Handles click events on active tags input for autocomplete.
   *
   * @param {Event} e
   *   The click event object.
   * @param {object} activeTags
   *   The activeTags for the active-tags widget.
   * @param {string} input
   *   The input for the active-tags widget.
   * @param {string} identifier
   *   The identifier for the active-tags widget.
   * @param {number} cardinality
   *   The maximum number of tags allowed.
   */
  function handleClickEvent(e, activeTags, input, identifier, cardinality) {
    // Check if the clicked target is the active tags input.
    const isActiveTagsInput = e.target.classList.contains('active-tags__input');
    // Check if the target is within the desired container.
    const isDesiredContainer = e.target.closest(`.${identifier}`);

    // Trigger autocomplete if conditions are met.
    if (isActiveTagsInput && isDesiredContainer) {
      handleAutocomplete(
        '',
        activeTags,
        activeTags.value.map((item) => item.entity_id),
        input,
        identifier,
        cardinality,
      );
    }
  }

  /**
   * Handles autocomplete functionality for the input field using Active Tags.
   *
   * @param {string} value
   *   The current value of the input field.
   * @param {object} activeTags
   *   An object of active tags.
   * @param {string[]} selectedEntities
   *   An array of selected entities.
   * @param {string} input
   *   The input for the active-tags widget.
   * @param {string} identifier
   *   The identifier for the active-tags widget.
   * @param {number} cardinality
   *   The maximum number of tags allowed.
   */
  function handleAutocomplete(value, activeTags, selectedEntities, input, identifier, cardinality) {
    // Set the controller and whitelist for the active tags.
    let controller = this.controller;
    activeTags.whitelist = null;

    // Abort previous controller if exists.
    if (controller) {
      controller.abort();
    }
    controller = new AbortController();
    this.controller = controller;

    // Create Loading text markup if identifier is present.
    if (identifier) {
      createLoadingTextMarkup(identifier);
    }

    // Show loading animation if value is not empty.
    value !== '' ? activeTags.loading(true) : activeTags.loading(false);

    // Fetch the autocomplete data.
    fetch(
      `${$(input).attr('data-autocomplete-url')}?q=${encodeURIComponent(value)}&selected=${selectedEntities}`,
      { signal: controller.signal }
    )
      .then((res) => res.json())
      .then((newWhitelist) => {
        const newWhitelistData = [];

        // Process the fetched data and create new whitelist data.
        newWhitelist.forEach((current) => {
          newWhitelistData.push({
            value: current.entity_id,
            entity_id: current.entity_id,
            avatar: current.avatar ?? null,
            info_label: current.info_label,
            label: current.label,
            editable: current.editable,
            ...current.attributes,
          });
        });

        // Update the whitelist if data exists.
        if (newWhitelistData) {
          activeTags.whitelist = newWhitelistData;

          // Render the suggestion dropdown.
          activeTags.loading(false).dropdown.show(value);

          // Remove Loading text markup if identifier is present.
          if (identifier) {
            removeLoadingTextMarkup(identifier);
          }
        }

        // Show dropdown suggestion if tag limit is not reached.
        if (!isTagLimitReached(cardinality, identifier)) {
          activeTags.dropdown.show();
        }
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
      });
  }

  /**
   * Checks if the tag limit has been reached.
   *
   * @param {number} cardinality
   *   The maximum number of tags allowed.
   * @param {string} identifier
   *   The identifier for the active-tags widget.
   *
   * @return {boolean}
   *   True if the tag limit has been reached, otherwise false.
   */
  function isTagLimitReached(cardinality, identifier) {
    // Check if the number of selected tags is greater than
    // or equal to the cardinality.
    return cardinality > 0 && countSelectedTags(identifier) >= cardinality;
  }

  /**
   * Counts the number of selected tags.
   *
   * @param {string} identifier
   *   The identifier for the active-tags widget.
   *
   * @return {number}
   *   The number of selected tags.
   */
  function countSelectedTags(identifier) {
    // Select the element with the provided identifier.
    const tagsElement = document.querySelector(`.${identifier}`);

    // If the tags element exists, count the number of
    // tags with the class 'active-tags__tag'.
    if (tagsElement) {
      const tagElements = tagsElement.querySelectorAll('.active-tags__tag');
      return tagElements.length;
    }

    // If the tags element does not exist, return 0.
    return 0;
  }

  /**
   * Creates loading text markup if it doesn't exist.
   *
   * @param {string} identifier
   *   The identifier for the active-tags widget.
   */
  function createLoadingTextMarkup(identifier) {
    // Select the element with the provided identifier.
    const tagsElement = document.querySelector(`.${identifier}`);

    // Check if tagsElement exists.
    if (!tagsElement) return;

    // Check if the element with class 'loading-text' exists.
    const existingLoadingText = tagsElement.querySelector('.active-tags--loading-text');

    // If the loading text element doesn't exist, create and append it.
    if (!existingLoadingText) {
      const loadingText = document.createElement('div');
      loadingText.className = 'active-tags--loading-text hidden';
      loadingText.textContent = 'Loading...';
      tagsElement.appendChild(loadingText);
    }
  }

  /**
   * Removes loading text markup.
   *
   * @param {string} identifier
   *   The identifier for the active-tags widget.
   */
  function removeLoadingTextMarkup(identifier) {
    // Select the element with the provided identifier.
    const tagsElement = document.querySelector(`.${identifier}`);

    // If the tags element exists, find and remove the loading text element.
    if (tagsElement) {
      const loadingText = tagsElement.querySelector('.active-tags--loading-text');
      if (loadingText) {
        loadingText.remove();
      }
    }
  }

  /**
   * Generates HTML markup for an entity id.
   *
   * This function creates the HTML structure for displaying an entity ID
   * in the Active Tags widget. The entity ID is only displayed if the
   * input element has the `data-show-entity-id` attribute set to a
   * truthy value.
   *
   * @param {HTMLElement} input
   *   The input element that contains the dataset attributes.
   * @param {string} entityId
   *   The entity id to be displayed.
   *
   * @return {string}
   *   The HTML markup for the entity id or an empty string if the entity id
   *   should not be displayed.
   */
  function entityIdMarkup(input, entityId) {
    // Check if the input element is defined, showEntityId is enabled,
    // and entityId is provided.
    if (input && parseInt(input.dataset.showEntityId, 10) && entityId) {
      return `
      <div class="active-tags__tag__with-entity-id">
        <div class="active-tags__tag__entity-id-wrap">
          <span class="active-tags__tag__entity-id">${entityId}</span>
        </div>
      </div>
    `;
    }
    // Return an empty string if conditions are not met.
    return '';
  }

  /**
   * Generates HTML markup for an info label.
   *
   * @param {string} infoLabel
   *   The info label information.
   *
   * @return {string}
   *   The info label markup HTML.
   */
  function infoLabelMarkup(infoLabel) {
    // Return an empty string if infoLabel is not provided.
    if (!infoLabel) {
      return '';
    }

    // Generate the markup based on whether the info label
    // is an image or plain text.
    if (validImgSrc(infoLabel) || containsSVG(infoLabel)) {
      const imgMarkup = validImgSrc(infoLabel)
        ? `<img src="${infoLabel}" class="active-tags__info-label-img">`
        : infoLabel;

      return `
      <div class="active-tags__tag__info-label-wrap">
        <div class="active-tags__tag__info-label-image">
          ${imgMarkup}
        </div>
      </div>
    `;
    } else {
      return `
      <div class="active-tags__tag__info-label-wrap">
        <span class="active-tags__tag__info-label">${infoLabel}</span>
      </div>
    `;
    }
  }

  /**
   * Generates HTML markup for a user avatar.
   *
   * This function creates the HTML structure for displaying a user's avatar
   * in the Active Tags widget. The avatar is only displayed if the user avatar
   * URL is provided and showAvatar is enabled.
   *
   * @param {string} label
   *   The label associated with the avatar (usually the user's name).
   * @param {string} userAvatar
   *   The URL of the user's avatar image.
   * @param {boolean} showAvatar
   *   Flag indicating whether the avatar should be shown.
   *
   * @return {string}
   *   The HTML markup for the user avatar, or an empty string if conditions
   *   are not met.
   */
  function userAvatarMarkup(label, userAvatar, showAvatar) {
    // Return an empty string if userAvatar is not provided or
    // if showAvatar is not enabled.
    if (!userAvatar || !showAvatar) {
      return '';
    }

    // Return the HTML markup for the user avatar.
    return `
    <div class='active-tags__tag__avatar-wrap'>
      <img alt="${label}" src="${userAvatar}">
    </div>
  `;
  }

  /**
   * Generates HTML markup for a tag.
   *
   * @param {string} tagLabel
   *   The label for the tag.
   * @param {string} tagInfoLabel
   *   The info label information.
   * @param {string} tagEntityId
   *   The entity id.
   * @param {string} tagImage
   *   The image associated with the tag (e.g., user avatar).
   *
   * @return {string}
   *   The tag markup HTML.
   */
  function tagMarkup(tagLabel, tagInfoLabel, tagEntityId, tagImage) {
    // Determine the appropriate class for the tag label.
    let tagClass = 'active-tags__tag__text';

    // Determine the appropriate class for the tag items div.
    let tagItemsClass = 'active-tags__tag__items';
    if (tagEntityId) {
      tagItemsClass += ' active-tags__tag__items__with-entity-id';
    }

    // Generate the tag HTML markup.
    return `
    <div class="${tagItemsClass}">
      ${tagEntityId}
      ${tagImage}
      <span class="${tagClass}">${tagLabel}</span>
      ${tagInfoLabel}
    </div>
  `;
  }

  /**
   * Generates HTML markup for a tag based on the provided tagData.
   *
   * Creates the HTML structure for a tag in the Active Tags widget using
   * the given tagData, including label, entity ID, and additional attributes.
   *
   * @param {Object} tagData
   *   Data for the tag, including value, entity_id, class, etc.
   * @param {HTMLElement} input
   *   The input element that contains the dataset attributes.
   *
   * @return {string}
   *   The HTML markup for the generated tag.
   */
  function tagTemplate(tagData, input) {
    // Parse convertUppercase as an integer from the dataset.
    const convertUppercase = parseInt(input.dataset.convertUppercase, 10) || 0;

    // Avoid 'undefined' values on paste event.
    let label = tagData.label ?? tagData.value;

    // Capitalizes the first letter and lowers the rest.
    const capitalizeFirstLetter = (str) => {
      if (str.length === 0) {
        return str;
      }
      return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
    };

    // If convertUppercase is enabled, modify the label.
    if (convertUppercase) {
      label = capitalizeFirstLetter(label);
    }

    // Generate the tag HTML markup.
    return `
    <tag title="${label}"
         contenteditable="false"
         spellcheck="false"
         tabIndex="-1"
         class="active-tags__tag ${tagData.class ? tagData.class : ''}"
         ${this.getAttributes(tagData)}>
      <x title="Remove ${label}"
         class="active-tags__tag__removeBtn"
         role="button"
         aria-label="remove ${label} tag"
         tabindex="0">
      </x>
      ${tagMarkup(
      label,
      infoLabelMarkup(tagData.info_label),
      entityIdMarkup(input, tagData.entity_id),
      userAvatarMarkup(label, tagData.avatar, parseInt(input.dataset.showAvatar, 10))
    )}
    </tag>
  `;
  }

  /**
   * Generates HTML markup for a tag based on the provided tagData.
   *
   * This function creates the HTML structure for a tag in the Active Tags
   * widget using the given tagData, including text and additional attributes.
   *
   * @param {Object} tagData
   *   Data for the tag, including value, text, class, etc.
   *
   * @return {string}
   *   The HTML markup for the generated tag.
   */
  function tagSelectTemplate(tagData) {
    return `
    <tag title="${tagData.text}"
         contenteditable="false"
         spellcheck="false"
         tabIndex="-1"
         class="active-tags__tag ${tagData.class ? tagData.class : ''}"
         ${this.getAttributes(tagData)}>
      <x class="active-tags__tag__removeBtn"
         role="button"
         aria-label="remove tag"
         tabIndex="0">
      </x>
      <div class="active-tags__tag__items">
        <span class="active-tags__tag__text">${tagData.text}</span>
      </div>
    </tag>
  `;
  }

  /**
   * Generates HTML markup for a tag based on the provided tagData.
   *
   * This function creates the HTML structure for a tag in the Active Tags
   * widget using the given tagData, including text, entity ID, count, and
   * additional attributes.
   *
   * @param {Object} tagData
   *   Data for the tag, including value, entity_id, class, etc.
   * @param {HTMLElement} input
   *   The input element that contains the dataset attributes.
   *
   * @return {string}
   *   The HTML markup for the generated tag.
   */
  function tagFacetsTemplate(tagData, input) {
    // Generate HTML for the entity ID section if showEntityId is enabled.
    const entityIdDiv =
      parseInt(input.dataset.showEntityId, 10) && tagData.entity_id
        ? `<div class="active-tags__tag__with-entity-id">
             ${entityIdMarkup(input, tagData.entity_id)}
           <span class='active-tags__tag__text'>${tagData.text}</span>
           <span class="active-tags__tag__facets-count">
             ${tagData.count}
           </span>
         </div>`
        : `<div id="active-tags__tag__items">
           <span class='active-tags__tag__facets-text'>${tagData.text}</span>
           <span class="active-tags__tag__facets-count">
             ${tagData.count}
           </span>
         </div>`;

    // Generate the tag HTML markup.
    return `
    <tag title="${tagData.text}"
         contenteditable='false'
         spellcheck='false'
         tabIndex="0"
         class="active-tags__tag ${tagData.class ? tagData.class : ''}"
         ${this.getAttributes(tagData)}>
      <x id="active-tags__tag-remove-button"
         class='active-tags__tag__removeBtn'
         role='button'
         aria-label='remove tag'>
      </x>
      ${entityIdDiv}
    </tag>
  `;
  }

  /**
   * Generates HTML markup for a dropdown item based on the provided tagData.
   *
   * This function creates the HTML structure for a dropdown item in the Active
   * Tags widget using the given tagData, including value, text, etc.
   *
   * @param {Object} tagData
   *   Data for the tag, including value, text, etc.
   * @param {number} cardinality
   *   The maximum number of tags allowed.
   * @param {string} identifier
   *   The identifier for the active-tags widget.
   *
   * @return {string}
   *   The HTML markup for the generated dropdown item.
   */
  function dropdownItemTemplate(tagData, cardinality, identifier) {
    // Retrieve class names from the settings.
    const { classNames } = this.settings;

    // Check if the tag limit is not reached or if the mode is active.
    if (!isTagLimitReached(cardinality, identifier) || this.mode) {
      const dropdownItemClass = classNames.dropdownItem;
      const highlightedText = highlightMatchingLetters(
        tagData.text,
        this.state.inputText
      );

      // Return the HTML markup for the dropdown item.
      return `
      <div class="${dropdownItemClass}"
           value="${tagData.value}"
           tabindex="0"
           role="option">
        <div class="active-tags__dropdown__item-highlighted">${highlightedText}</div>
      </div>
    `;
    }
    // Return an empty string if the tag limit is reached
    // and mode is not active.
    return '';
  }

  /**
   * Generates HTML for a suggestion item in the Active Tags dropdown.
   *
   * Uses the provided tagData to create HTML for a suggestion item, including
   * text and count.
   *
   * @param {Object} tagData
   *   The data representing the suggestion item.
   *
   * @return {string}
   *   The HTML template for the suggestion item.
   */
  function suggestionFacetsItemTemplate(tagData) {
    // Generate the HTML template for the suggestion item.
    return `<div ${this.getAttributes(tagData)}
                class='active-tags__dropdown__item ${tagData.class ? tagData.class : ''}'
                tabindex="0"
                role="option">
            ${highlightMatchingLetters(tagData.text, this.state.inputText)}
            <span class="active-tags__tag-facets-count">
              ${tagData.count}
            </span>
          </div>`;
  }

  /**
   * Generates the HTML for a suggestion item.
   *
   * Constructs the HTML for a suggestion in the Active Tags dropdown based on
   * the provided tagData, which includes the item’s label, class, and other
   * attributes.
   *
   * @param {Object} tagData
   *   Data representing the suggestion item.
   * @param {number} cardinality
   *   The maximum number of tags allowed.
   * @param {string} identifier
   *   The identifier for the active-tags widget.
   * @param {number} showAvatar
   *   Indicates whether to show the avatar for the tag.
   *
   * @return {string}
   *   HTML template for the suggestion item.
   */
  function suggestionItemTemplate(tagData, cardinality, identifier, showAvatar) {
    // Return suggestion item when the field cardinality is unlimited or
    // field cardinality is bigger than the number of selected tags.
    if (!isTagLimitReached(cardinality, identifier)) {
      return `
      <div ${this.getAttributes(tagData)}
           class="active-tags__dropdown__item ${tagData.class ? tagData.class : ''}"
           tabindex="0"
           role="option">
        ${tagData.avatar && showAvatar
        ? `<div class='active-tags__dropdown__item__avatar-wrap'>
               <img src="${tagData.avatar}">
             </div>`
        : ''
      }
        <div class="active-tags__dropdown__item-highlighted ${tagData.avatar && showAvatar ? 'active-tags__dropdown-user-info' : ''}">
          ${highlightMatchingLetters(tagData.label, this.state.inputText)}
        </div>
        ${infoLabelMarkup(tagData.info_label)}
      </div>
    `;
    }
    // Return an empty string if the tag limit is reached.
    return '';
  }

  /**
   * Generates the HTML for the suggestion footer.
   *
   * Constructs the HTML for the footer in the Active Tags dropdown based on
   * the provided cardinality and identifier, displaying a message when the
   * tag limit has been reached.
   *
   * @param {Object} tagData
   *   Data representing the suggestion item.
   * @param {number} cardinality
   *   The maximum number of tags allowed.
   * @param {string} identifier
   *   The identifier for the active-tags widget.
   *
   * @return {string}
   *   HTML template for the suggestion footer.
   */
  function suggestionFooterTemplate(tagData, cardinality, identifier) {
    // Returns empty dropdown footer when field cardinality is unlimited or
    // field cardinality is bigger than the number of selected tags.
    if (isTagLimitReached(cardinality, identifier)) {
      return `
      <footer data-selector="active-tags-suggestions-footer" class="${this.settings.classNames.dropdownFooter}">
        <p>${Drupal.t(drupalSettings.activeTagsAutocomplete[$(input).attr("id")].limit_tag_message, {"@cardinality": cardinality})}</p>
      </footer>
    `;
    }
    // Return an empty string if the tag limit is not reached.
    return '';
  }

  /**
   * Generates the HTML for the suggestion footer.
   *
   * Constructs the HTML for the footer in the Active Tags dropdown based on
   * the provided cardinality and identifier, displaying a message when the tag
   * limit has been reached.
   *
   * @param {Object} tagData
   *   Data representing the suggestion item.
   * @param {number} cardinality
   *   The maximum number of tags allowed.
   * @param {string} identifier
   *   The identifier for the active-tags widget.
   *
   * @return {string}
   *   The HTML template for the suggestion footer.
   */
  function suggestionFooterSelectTemplate(tagData, cardinality, identifier) {
    // Extract classNames from the settings.
    const { classNames } = this.settings;

    // Returns empty dropdown footer when field cardinality is unlimited or
    // field cardinality is bigger than the number of selected tags.
    if (isTagLimitReached(cardinality, identifier) && !this.mode) {
      return `
      <footer data-selector="active-tags-suggestions-footer" class="${classNames.dropdownFooter}">
        <p>${Drupal.t(drupalSettings.activeTagsSelect[$(select).attr("id")].limit_tag_message, {"@cardinality": cardinality})}</p>
      </footer>
    `;
    }

    // Return an empty string if the tag limit is not reached or mode is set.
    return '';
  }

  /**
   * Highlights matching letters in a string.
   *
   * This function takes an input string and a search term, then highlights
   * all occurrences of the search term within the input string by wrapping
   * them in <strong> tags. This is useful for emphasizing matched terms
   * in search results or autocomplete suggestions.
   *
   * @param {string} inputTerm
   *   The input string where the search is performed.
   * @param {string} searchTerm
   *   The term to highlight within the input string.
   *
   * @return {string}
   *   The input string with matching letters wrapped in <strong> tags.
   */
  function highlightMatchingLetters(inputTerm, searchTerm) {
    // Escape special characters in the search term to
    // prevent issues with RegExp.
    const escapedSearchTerm = searchTerm.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

    // Create a regular expression to match the search term globally
    // and case insensitively.
    const regex = new RegExp(`(${escapedSearchTerm})`, 'gi');

    // Check if the search term is empty and return
    // the original string if it is.
    if (!escapedSearchTerm) {
      return inputTerm;
    }

    // Replace matching letters with the same letters wrapped in <strong> tags.
    return inputTerm.replace(regex, '<strong>$1</strong>');
  }

  /**
   * Checks if a given string contains SVG elements.
   *
   * This function takes a string and checks if it contains any SVG elements by
   * creating a temporary DOM element and setting its innerHTML to the provided
   * string. It then queries the DOM element for any SVG elements.
   *
   * @param {string} infoLabel
   *   The string to be checked for SVG elements.
   *
   * @return {boolean}
   *   Returns true if the string contains SVG elements, otherwise false.
   */
  function containsSVG(infoLabel) {
    // Check if infoLabel is defined and is a string.
    if (infoLabel && typeof infoLabel === 'string') {
      // Create a temporary div element.
      const tempDiv = document.createElement('div');
      // Set the innerHTML of the div to the content of infoLabel.
      tempDiv.innerHTML = infoLabel;
      // Check if the div contains any SVG elements.
      const svgElements = tempDiv.querySelectorAll('svg');
      // Return true if any SVG elements are found, false otherwise.
      return svgElements.length > 0;
    }
    // Return false if infoLabel is not defined or not a string.
    return false;
  }

  /**
   * Checks if the info label is a valid image source.
   *
   * This function uses a regular expression to test if the provided string is
   * a valid URL for an image file. Supported image extensions include jpg,
   * jpeg, png, gif, bmp, svg, and webp.
   *
   * @param {string} infoLabel
   *   The info label input.
   *
   * @return {boolean}
   *   True if the info label is a valid image source.
   */
  function validImgSrc(infoLabel) {
    // Define the regular expression pattern for a valid image URL.
    const pattern = new RegExp(
      '^(https?:\\/\\/)?' +
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' +
      '((\\d{1,3}\\.){3}\\d{1,3}))' +
      '(\\:\\d+)?' +
      '(\\/[-a-z\\d%_.~+]*)*' +
      '(\\.(?:jpg|jpeg|png|gif|bmp|svg|webp))' +
      '(\\?[;&a-z\\d%_.~+=-]*)?' +
      '(\\#[-a-z\\d_]*)?$',
      'i'
    );
    // Test the infoLabel against the pattern and return the result.
    return !!pattern.test(infoLabel);
  }

  /**
   * Attaches the behavior for initializing Active Tags Autocomplete widget.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the Active Tags Autocomplete widget.
   */
  Drupal.behaviors.activeTagsWidgets = {
    input: null,
    select: null,
    facet: null,
    mode: null,
    controller: null,
    activeTags: null,
    activeTagsSelect: null,
    activeTagsFacet: null,
    attach: function (context, settings) {
      // Initialize Active Tags on specific elements.
      once('active-tags-widget', 'input.active-tags-widget', context).forEach(
        initAutocomplete,
      );
      once('active-tags-select-widget', 'select.active-tags-select-widget', context).forEach(
        initSelect,
      );
      once('active-tags-facets-widget', '.active-tags-facets-widget', context).forEach(
        initFacets,
      );
    }
  };

})(jQuery, Drupal, drupalSettings, Drupal.activeTags, Drupal.debounce, window.JSON, Sortable, once);
