<?php

namespace Drupal\active_tags\Controller;

use Drupal\active_tags\ActiveTagsEntityAutocompleteMatcher;
use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Site\Settings;
use Drupal\system\Controller\EntityAutocompleteController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Defines a route controller for entity autocomplete form elements.
 */
class ActiveTagsEntityAutocompleteController extends EntityAutocompleteController {

  /**
   * The autocomplete matcher for entity references.
   *
   * @var \Drupal\active_tags\ActiveTagsEntityAutocompleteMatcher
   */
  protected $matcher;

  /**
   * Constructs an ActiveTagsEntityAutocompleteMatcher object.
   *
   * @param \Drupal\active_tags\ActiveTagsEntityAutocompleteMatcher $matcher
   *   The matcher.
   */
  public function __construct(ActiveTagsEntityAutocompleteMatcher $matcher) {
    $this->matcher = $matcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('active_tags.autocomplete_matcher')
    );
  }

  /**
   * Autocomplete the label of an entity.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object that contains the typed tags.
   * @param string $target_type
   *   The ID of the target entity type.
   * @param string $selection_handler
   *   The plugin ID of the entity reference selection handler.
   * @param string $selection_settings_key
   *   The hashed key of the key/value entry that holds the selection handler
   *   settings.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The matched entity labels as a JSON response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown if the selection settings key is not found in the key/value store
   *   or if it does not match the stored data.
   */
  public function handleAutocomplete(Request $request, $target_type, $selection_handler, $selection_settings_key) {
    $matches = [];
    // Get already selected entity ids.
    $selected = $request->query->get('selected')
      ? explode(',', $request->query->get('selected'))
      : [];

    // Selection settings are passed in as a hashed key of a serialized array
    // stored in the key/value store.
    $selection_settings = $this->keyValue('entity_autocomplete')->get($selection_settings_key, FALSE);

    // Get the typed string from the URL, if it exists.
    $input = trim($request->query->get('q'));
    if (!empty($input)) {
      $tag_list = Tags::explode($input);

      if ($selection_settings !== FALSE) {
        $selection_settings_hash = Crypt::hmacBase64(serialize($selection_settings) . $target_type . $selection_handler, Settings::getHashSalt());
        if (!hash_equals($selection_settings_hash, $selection_settings_key)) {
          // Disallow access when the selection settings hash does not match the
          // passed-in key.
          throw new AccessDeniedHttpException('Invalid selection settings key.');
        }
      }
      else {
        // Disallow access when the selection settings key is not found in the
        // key/value store.
        throw new AccessDeniedHttpException();
      }
    }
    else {
      // Validate the autocomplete minimum length.
      if ($selection_settings['min_length']) {
        return new JsonResponse($matches);
      }
    }

    $typed_string = !empty($tag_list) ? mb_strtolower(array_pop($tag_list)) : '';
    $matches = $this->matcher->getMatches($target_type, $selection_handler, $selection_settings, $typed_string, $selected);

    return new JsonResponse($matches);
  }

}
