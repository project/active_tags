<?php

namespace Drupal\active_tags\Element;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Textfield;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides an active tags entity autocomplete form element.
 *
 * The autocomplete form element allows users to select one or multiple
 * entities, which can come from all or specific bundles of an entity type.
 *
 * Properties:
 *  - #target_type: (required) The ID of the target entity type.
 *  - #tags: (optional) TRUE if the element allows multiple selection. Defaults
 *    to FALSE.
 *  - #default_value: (optional) The default entity or an array of default
 *    entities, depending on the value of #tags.
 *  - #selection_handler: (optional) The plugin ID of the entity reference
 *    selection handler (a plugin of type EntityReferenceSelection). The default
 *    value is the lowest-weighted plugin that is compatible with #target_type.
 *  - #selection_settings: (optional) An array of settings for the selection
 *    handler. Settings for the default selection handler
 *    \Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection are:
 *    - target_bundles: Array of bundles to allow (omit to allow all bundles).
 *    - sort: Array with 'field' and 'direction' keys, determining how results
 *      will be sorted. Defaults to unsorted.
 *  - #autocreate: (optional) Array of settings used to auto-create entities
 *    that do not exist (omit to not auto-create entities). Elements:
 *    - bundle: (required) Bundle to use for auto-created entities.
 *    - uid: User ID to use as the author of auto-created entities. Defaults to
 *      the current user.
 *  - #process_default_value: (optional) Set to FALSE if the #default_value
 *    property is processed and access checked elsewhere (such as by a Field API
 *    widget). Defaults to TRUE.
 *  - #validate_reference: (optional) Set to FALSE if validation of the selected
 *    entities is performed elsewhere. Defaults to TRUE.
 *
 *  Usage example:
 * @code
 *  $form['my_element'] = [
 *   '#type' => 'entity_autocomplete_active_tags',
 *   '#target_type' => 'node',
 *   '#tags' => TRUE,
 *   '#default_value' => $entities,
 *   '#selection_handler' => 'default',
 *   '#selection_settings' => [
 *     'target_bundles' => ['article', 'page'],
 *     'info_label' => [node:type],
 *    ],
 *   '#autocreate' => [
 *     'bundle' => 'article',
 *     'uid' => <a valid user ID>,
 *    ],
 *  ];
 * @endcode
 *
 * @see \Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection
 *
 * @FormElement("entity_autocomplete_active_tags")
 */
class EntityAutocompleteActiveTags extends Textfield {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $class = static::class;

    // Apply default form element properties.
    $info['#target_type'] = NULL;
    $info['#selection_handler'] = 'default';
    $info['#selection_settings'] = [];
    $info['#tags'] = TRUE;
    $info['#autocreate'] = NULL;
    $info['#max_items'] = 10;
    $info['#min_length'] = 1;
    $info['#match_operator'] = 'CONTAINS';
    $info['#style'] = 'rectangle';
    $info['#show_entity_id'] = 0;
    $info['#show_avatar'] = 0;
    $info['#convert_uppercase'] = 0;
    $info['#string_override'] = 0;
    $info['#info_label'] = '';
    $info['#identifier'] = '';
    // This should only be set to FALSE if proper validation by the selection
    // handler is performed at another level on the extracted form values.
    $info['#validate_reference'] = TRUE;
    // IMPORTANT! This should only be set to FALSE if the #default_value
    // property is processed at another level (e.g. by a Field API widget) and
    // its value is properly checked for access.
    $info['#process_default_value'] = TRUE;
    array_unshift($info['#process'], [$class, 'processActiveTagsEntityAutocomplete']);

    return $info;
  }

  /**
   * Adds active tags entity autocomplete functionality to a form element.
   *
   * @param array $element
   *   The form element to process. Properties used:
   *   - #target_type: The ID of the target entity type.
   *   - #selection_handler: The plugin ID of the entity reference selection
   *     handler.
   *   - #selection_settings: An array of settings that will be passed to the
   *     selection handler.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The form element.
   *
   * @throws \InvalidArgumentException
   *   Exception thrown when the #target_type or #autocreate['bundle'] are
   *   missing.
   */
  public static function processActiveTagsEntityAutocomplete(array &$element, FormStateInterface $form_state, array &$complete_form) {
    // Nothing to do if there is no target entity type.
    if (empty($element['#target_type'])) {
      throw new \InvalidArgumentException('Missing required #target_type parameter.');
    }

    // Provide default values and sanity checks for the #autocreate parameter.
    if ($element['#autocreate']) {
      if (!isset($element['#autocreate']['bundle'])) {
        throw new \InvalidArgumentException("Missing required #autocreate['bundle'] parameter.");
      }
      // Default the autocreate user ID to the current user.
      $element['#autocreate']['uid'] = $element['#autocreate']['uid'] ?? \Drupal::currentUser()->id();
    }

    // Do not attach js library if the element is disabled.
    $element_disabled = $element['#disabled'] ?? FALSE;
    if (!$element_disabled) {
      $element['#attached']['library'][] = 'active_tags/assets';

      if (_active_tags_is_gin_theme_active()) {
        // Overrides to support Gin's CSS3 variables for Dark-mode, Accent etc.
        $element['#attached']['library'][] = 'active_tags/assets.gin';
      }

      if (_active_tags_is_claro_theme_active()) {
        // Workaround for problems with jquery css in claro theme.
        $element['#attached']['library'][] = 'active_tags/assets.claro';
      }

      if (_active_tags_is_seven_theme_active()) {
        // Workaround for problems with jquery css in seven theme.
        $element['#attached']['library'][] = 'active_tags/assets.seven';
      }
    }

    // Store the selection settings in the key/value store and pass a hashed key
    // in the route parameters.
    $selection_settings = $element['#selection_settings'] ?? [];
    // Don't serialize the entity, it will be added explicitly afterward.
    if (isset($selection_settings['entity']) && ($selection_settings['entity'] instanceof EntityInterface)) {
      $element['#autocomplete_query_parameters']['entity_type'] = $selection_settings['entity']->getEntityTypeId();
      $element['#autocomplete_query_parameters']['entity_id'] = $selection_settings['entity']->id();
      unset($selection_settings['entity']);
    }
    $data = serialize($selection_settings) . $element['#target_type'] . $element['#selection_handler'];
    $selection_settings_key = Crypt::hmacBase64($data, Settings::getHashSalt());

    $key_value_storage = \Drupal::keyValue('entity_autocomplete');
    if (!$key_value_storage->has($selection_settings_key)) {
      $key_value_storage->set($selection_settings_key, $selection_settings);
    }

    $element['#autocomplete_route_name'] = 'system.entity_autocomplete';
    $element['#autocomplete_route_parameters'] = [
      'target_type' => $element['#target_type'],
      'selection_handler' => $element['#selection_handler'],
      'selection_settings_key' => $selection_settings_key,
    ];

    if (!isset($element['#attributes']['class'])) {
      $element['#attributes']['class'] = [];
    }
    if (!isset($element['#style'])) {
      $element['#style'] = 'rectangle';
    }
    $element['#attributes']['class'][] = 'active-tags-widget' . ' active-tags__tag__' . $element['#style'];

    $element['#attributes']['data-max-items'] = $element['#match_limit'] ?? 10;
    $element['#attributes']['data-suggestions-dropdown'] = $element['#min_length'] ?? '';
    $element['#attributes']['data-match-operator'] = ($element['#match_operator'] === 'CONTAINS') ? 1 : 0;
    $element['#attributes']['data-placeholder'] = $element['#placeholder'] ?? '';
    $element['#attributes']['data-show-entity-id'] = $element['#show_entity_id'] ?? '';
    $element['#attributes']['data-identifier'] = $element['#identifier'] ?? '';
    $element['#attributes']['data-cardinality'] = $element['#cardinality'] ?? '';

    if ($element['#autocreate'] && isset($element['#convert_uppercase'])) {
      $element['#attributes']['data-convert-uppercase'] = $element['#convert_uppercase'];
    }

    if ($element['#target_type'] == 'user' && isset($element['#show_avatar'])) {
      $element['#attributes']['data-show-avatar'] = $element['#show_avatar'];
    }

    // Store the selection settings in the key/value store and pass a hashed key
    // in the route parameters.
    $selection_settings = $element['#selection_settings'] ?? [];
    $data = serialize($selection_settings) . $element['#target_type'] . $element['#selection_handler'];
    $selection_settings_key = Crypt::hmacBase64($data, Settings::getHashSalt());

    $key_value_storage = \Drupal::keyValue('entity_autocomplete');
    if (!$key_value_storage->has($selection_settings_key)) {
      $key_value_storage->set($selection_settings_key, $selection_settings);
    }

    $element['#attributes']['data-autocomplete-url'] = Url::fromRoute('active_tags.entity_autocomplete', [
      'target_type' => $element['#target_type'],
      'selection_handler' => $element['#selection_handler'],
      'selection_settings_key' => $selection_settings_key,
    ])->toString();

    // Set default options for multiple values.
    $element['#cardinality'] = $element['#cardinality'] ?? -1;

    $js_settings[$element['#id']] = [
      'input_id' => $element['#id'],
      'cardinality' => $element['#cardinality'],
      'required' => $element['#required'],
      'limit' => $element['#limit'] ?? 10,
      'min_length' => $element['#min_length'] ?? 0,
      'use_synonyms' => $element['#use_synonyms'] ?? 0,
      'delimiter' => $element['#delimiter'] ?? '',
      'new_terms' => $element['#new_terms'] ?? FALSE,
      'string_override' => $element['#string_override'] ?? 0,
      'not_exist_message' => $element['#not_exist_message'] ?? NULL,
      'not_found_message' => $element['#not_found_message'] ?? NULL,
      'limit_tag_message' => $element['#limit_tag_message'] ?? "Allowed number of values limited to <strong>@cardinality</strong>.",
    ];

    if (!$element_disabled) {
      $element['#attached']['drupalSettings']['activeTagsAutocomplete'] = $js_settings;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    // Process the #default_value property.
    if ($input === FALSE && isset($element['#default_value']) && $element['#default_value']) {
      // Extract the labels from the passed-in entity objects, taking access
      // checks into account.
      return static::getDefaultValue($element['#default_value'], $element['#info_label'] ?? '');
    }

    // Potentially the #value is set directly, so it contains the 'target_id'
    // array structure instead of a string.
    if ($input !== FALSE && is_array($input)) {
      $entity_ids = array_map(function (array $item) {
        return $item['target_id'];
      }, $input);
      $entities = \Drupal::entityTypeManager()->getStorage($element['#target_type'])->loadMultiple($entity_ids);

      return static::getDefaultValue($entities, $element['#info_label'] ?? '');
    }

    return NULL;
  }

  /**
   * Form API after build callback for the duration parameter type form.
   *
   * Fixes up the form value by applying the multiplier.
   */
  public static function afterBuild(array $element, FormStateInterface $form_state) {
    // By default, Drupal sets the maxlength to 128 if the property isn't
    // specified, but since the limit isn't useful in some cases,
    // we unset the property.
    unset($element['textfield']['#maxlength']);

    // Set the elements value from either the value field or text field input.
    $element['#value'] = isset($element['value_field']) ? $element['value_field']['#value'] : $element['textfield']['#value'];

    if (isset($element['value_field'])) {
      $element['#value'] = trim($element['#value']);
      // Replace all cases of double quotes and one or more spaces with a
      // delimiter. This will allow us to keep entries in double quotes.
      $element['#value'] = preg_replace('/"" +""/', $element['#delimiter'] ?? ',', $element['#value']);
      // Remove the double quotes at the beginning and the end from the first
      // and the last term.
      $element['#value'] = substr($element['#value'], 2, strlen($element['#value']) - 4);

      unset($element['value_field']['#maxlength']);
    }

    $form_state->setValueForElement($element, $element['#value']);

    return $element;
  }

  /**
   * Formats the default values array for the active-tags widget.
   *
   * This method is also responsible for checking the 'view label'
   * access on the passed-in entities.
   *
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   An array of entity objects.
   * @param string $info_label_template
   *   The extra information that will be shown next to the entity label.
   *
   * @return false|string
   *   The active-tags default values array. Associative array with at least the
   *   following keys:
   *   - 'value':
   *     The referenced entity ID, IE: 1.
   *   - 'label':
   *     The text to be shown in the autocomplete and Active-tags,
   *     IE: "My label"
   *   - 'entity_id':
   *     The referenced entity ID, IE: 1.
   *   - 'info_label':
   *     The extra information to be shown next to the entity label.
   */
  public static function getDefaultValue(array $entities, string $info_label_template) {
    /** @var \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository */
    $entity_repository = \Drupal::service('entity.repository');
    $default_value = [];

    foreach ($entities as $entity) {
      // Set the entity in the correct language for display.
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $entity_repository->getTranslationFromContext($entity);
      $entity_id = $entity->id();

      // Use the special view label, since some entities allow the label to be
      // viewed, even if the entity is not allowed to be viewed.
      $label = $entity->access('view label') ? $entity->label() : t('- Restricted access -');

      // Replace tokens in the info label template.
      $info_label = \Drupal::token()->replacePlain(
        $info_label_template,
        [$entity->getEntityTypeId() => $entity],
        ['clear' => TRUE]
      );
      $info_label = trim(preg_replace('/\s+/', ' ', $info_label));

      // Allow other modules to alter the label and info_label.
      $context = ['entity' => $entity, 'info_label' => $info_label_template];
      \Drupal::moduleHandler()->alter('active_tags_autocomplete_match', $label, $info_label, $context);

      if ($label === NULL) {
        continue;
      }

      // Initialize the default value item array.
      $default_value_item = [
        'value' => $entity_id,
        'label' => $label,
        'entity_id' => $entity_id,
        'info_label' => $info_label,
        'editable' => FALSE,
      ];

      // Add the avatar URL if the entity type is 'user'.
      if ($entity->getEntityTypeId() === 'user') {
        $image_url = '';
        /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
        if ($entity->hasField('user_picture') && !$entity->get('user_picture')->isEmpty()) {
          /** @var \Drupal\file\FileInterface $user_image */
          $user_image = $entity->get('user_picture')->entity;
          $image_style = 'thumbnail';
          /** @var \Drupal\image\Entity\ImageStyle $style */
          $style = ImageStyle::load($image_style);
          $image_url = $style->buildUrl($user_image->getFileUri());
        }
        $active_tags_path = \Drupal::service('extension.list.module')->getPath('active_tags');
        $default_value_item['avatar'] = $image_url ?: '/' . $active_tags_path . '/assets/images/no-user.svg';
      }

      // Add the default value item to the default value array.
      $default_value[] = $default_value_item;
    }

    // Return the JSON-encoded default value array.
    return json_encode($default_value);
  }

}
