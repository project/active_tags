<?php

namespace Drupal\active_tags\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implementation of the 'entity_reference_autocomplete_active_tags' widget.
 *
 * @FieldWidget(
 *    id = "entity_reference_autocomplete_active_tags",
 *    label = @Translation("Autocomplete (Active Tags)"),
 *    description = @Translation("An autocomplete text field with active tags support."),
 *    field_types = {
 *      "entity_reference"
 *    },
 *    multiple_values = TRUE
 *  )
 */
class EntityReferenceAutocompleteActiveTagsWidget extends EntityReferenceAutocompleteWidget {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The selection plugin manager.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected $selectionManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selection_manager
   *   The selection plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, AccountInterface $current_user, SelectionPluginManagerInterface $selection_manager, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->currentUser = $current_user;
    $this->selectionManager = $selection_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('plugin.manager.entity_reference_selection'),
      $container->get('entity_type.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'autocomplete_route_name' => 'active_tags.entity_autocomplete',
      'match_operator' => 'CONTAINS',
      'style' => 'rectangle',
      'size' => 60,
      'selection_handler' => 'default',
      'match_limit' => 10,
      'min_length' => 1,
      'delimiter' => '',
      'placeholder' => '',
      'show_entity_id' => 0,
      'show_avatar' => 1,
      'convert_uppercase' => 0,
      'string_override' => 0,
      'show_info_label' => 0,
      'info_label' => '',
      'not_exist_message' => NULL,
      'not_found_message' => NULL,
      'limit_tag_message' => "Allowed number of values limited to <strong>@cardinality</strong>.",
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $entity_type = $this->fieldDefinition->getFieldStorageDefinition()->getSetting('target_type');
    $auto_create = $this->getSelectionHandlerSetting('auto_create');
    $element['style'] = [
      '#type' => 'select',
      '#title' => $this->t('Tag style'),
      '#default_value' => $this->getSetting('style'),
      '#options' => $this->getTagStyleOptions(),
      '#description' => $this->t('Choose the style for displaying tags from the available options.'),
    ];
    $element['match_operator'] = [
      '#type' => 'radios',
      '#title' => $this->t('Autocomplete matching'),
      '#default_value' => $this->getSetting('match_operator'),
      '#options' => $this->getMatchOperatorOptions(),
      '#description' => $this->t('Select the method used to collect autocomplete suggestions. Note that <em>Contains</em> can cause performance issues on sites with thousands of entities.'),
    ];
    $element['match_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of results'),
      '#default_value' => $this->getSetting('match_limit'),
      '#min' => 0,
      '#description' => $this->t('Specify the number of suggestions to display. Use <em>0</em> to show unlimited results.'),
    ];
    $element['min_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum length'),
      '#description' => $this->t('Set the minimum number of characters needed to trigger the suggestion list.'),
      '#default_value' => $this->getSetting('min_length'),
    ];
    $element['delimiter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delimiter'),
      '#description' => $this->t('Define a character to separate terms, in addition to the Enter key.'),
      '#default_value' => $this->getSetting('delimiter'),
      '#size' => 1,
    ];
    $element['show_entity_id'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include entity id'),
      '#default_value' => $this->getSetting('show_entity_id'),
      '#description' => $this->t('Enable this to include the entity ID within each tag.'),
    ];
    if ($entity_type == 'user') {
      $element['show_avatar'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Display user avatar'),
        '#description' => $this->t("Enable to display the user's avatar before each tag."),
        '#default_value' => $this->getSetting('show_avatar'),
      ];
    }
    if ($auto_create) {
      $element['convert_uppercase'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Capitalize first letter'),
        '#description' => $this->t('Converts the first letter of new entity labels to uppercase.'),
        '#default_value' => $this->getSetting('convert_uppercase'),
      ];
    }
    $element['string_override'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('String override'),
      '#description' => $this->t('Enable this option to customize the messages for "Not exist message" and "Not found message".'),
      '#default_value' => $this->getSetting('string_override'),
    ];
    $element['not_exist_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Not exist message'),
      '#description' => $this->t("Message to display when the term doesn't exist but can be created."),
      '#default_value' => $this->getSetting('not_exist_message'),
      '#states' => [
        'visible' => [
          sprintf(':input[name="fields[%s][settings_edit_form][settings][string_override]"]', $this->fieldDefinition->getName()) => ['checked' => TRUE],
        ],
      ],
    ];
    $element['not_found_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Not found message'),
      '#description' => $this->t("Message to display when the term doesn't exist and can't be created."),
      '#default_value' => $this->getSetting('not_found_message'),
      '#states' => [
        'visible' => [
          sprintf(':input[name="fields[%s][settings_edit_form][settings][string_override]"]', $this->fieldDefinition->getName()) => ['checked' => TRUE],
        ],
      ],
    ];
    $element['show_info_label'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include info label'),
      '#default_value' => $this->getSetting('show_info_label'),
      '#description' => $this->t('Enable this to show an additional label with information next to the entity label.'),
    ];
    $element['info_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Info label content'),
      '#default_value' => $this->getSetting('info_label'),
      '#description' => $this->t('The content to display in the additional label. Use tokens for dynamic values.'),
      '#states' => [
        'visible' => [
          sprintf(':input[name="fields[%s][settings_edit_form][settings][show_info_label]"]', $this->fieldDefinition->getName()) => ['checked' => TRUE],
        ],
      ],
    ];
    if ($this->moduleHandler->moduleExists('token')) {
      // Convert 'taxonomy_term' target type into 'term' in order to make it
      // work as a token type.
      if ($entity_type === 'taxonomy_term') {
        $entity_type = 'term';
      }
      $element['info_label_tokens'] = [
        '#type' => 'item',
        '#theme' => 'token_tree_link',
        '#token_types' => [$entity_type],
        '#show_restricted' => TRUE,
        '#global_types' => FALSE,
        '#recursion_limit' => 3,
        '#states' => [
          'visible' => [
            sprintf(':input[name="fields[%s][settings_edit_form][settings][show_info_label]"]', $this->fieldDefinition->getName()) => ['checked' => TRUE],
          ],
        ],
      ];
    }
    $element['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => $this->t('Enter the text to display inside the field before a value is entered, typically a sample value or brief description of the expected format.'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $styles = $this->getTagStyleOptions();
    $summary[] = $this->t('Tag style: @tag_style', ['@tag_style' => $styles[$this->getSetting('style')]]);

    $operators = $this->getMatchOperatorOptions();
    $summary[] = $this->t('Autocomplete matching: @match_operator', ['@match_operator' => $operators[$this->getSetting('match_operator')]]);

    $size = $this->getSetting('match_limit') ?: $this->t('unlimited');
    $summary[] = $this->t('Autocomplete suggestion list size: @size', ['@size' => $size]);

    $summary[] = $this->t('Min length: @min_length', ['@min_length' => $this->getSetting('min_length')]);
    $summary[] = $this->t('Delimiter: @delimiter', ['@delimiter' => $this->getSetting('delimiter')]);

    $show_entity_id = $this->getSetting('show_entity_id');
    $summary[] = $show_entity_id ? $this->t('Include the entity ID within the tag') : $this->t('Remove the entity ID from the tag');

    $entity_type = $this->fieldDefinition->getFieldStorageDefinition()->getSetting('target_type');
    if ($entity_type == 'user') {
      $show_avatar = $this->getSetting('show_avatar');
      $summary[] = $show_avatar ? $this->t("Display the user's avatar before each tag") : $this->t("Hide the user's avatar from the tag");
    }

    $auto_create = $this->getSelectionHandlerSetting('auto_create');
    if ($auto_create) {
      $summary[] = $this->t('Capitalize first letter: @convert_uppercase', ['@convert_uppercase' => $this->getSetting('convert_uppercase') ? $this->t('On') : $this->t('Off')]);
    }

    if ($this->getSetting('string_override')) {
      $summary[] = $this->t('Not exist message: @not_exist_message', ['@not_exist_message' => $this->getSetting('not_exist_message')]);
      $summary[] = $this->t('Not found message: @not_found_message', ['@not_found_message' => $this->getSetting('not_found_message')]);
    }

    if ($this->getSetting('show_info_label')) {
      $summary[] = $this->t('Include extra information within the tag.');
    }

    $placeholder = $this->getSetting('placeholder');
    if (!empty($placeholder)) {
      $summary[] = $this->t('Placeholder: @placeholder', ['@placeholder' => $placeholder]);
    }
    else {
      $summary[] = $this->t('No placeholder');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $entity = $items->getEntity();
    $referenced_entities = $items->referencedEntities();

    $selection_settings = [];
    // Append the match operation to the selection settings.
    if ($this->getFieldSetting('handler_settings') !== NULL) {
      $selection_settings = $this->getFieldSetting('handler_settings') + [
        'handler' => $this->getFieldSetting('handler'),
        'match_operator' => $this->getSetting('match_operator'),
        'match_limit' => $this->getSetting('match_limit') ?? 10,
        'min_length' => $this->getSetting('min_length'),
        'placeholder' => $this->getSetting('placeholder'),
        'cardinality' => $this->fieldDefinition
          ->getFieldStorageDefinition()
          ->getCardinality(),
        'show_entity_id' => (bool) $this->getSetting('show_entity_id'),
      ];
    }
    $selection_settings += [
      'match_operator' => $this->getSetting('match_operator'),
      'match_limit' => $this->getSetting('match_limit'),
    ];

    // Append the entity if it is already created.
    if (!$entity->isNew()) {
      $selection_settings['entity'] = $entity;
    }

    if ($this->getSetting('show_info_label')) {
      $selection_settings['info_label'] = $this->getSetting('info_label');
    }

    // User field definition doesn't have fieldStorage defined.
    $target_type = $this->getFieldSetting('target_type');
    $cardinality = $target_type !== 'user'
      ? $items->getFieldDefinition()->getFieldStorageDefinition()->isMultiple()
      : '';

    // Handle field cardinality in the Active Tags side.
    $limited = !$cardinality ? 'active-tags--limited' : '';
    $auto_create = $this->getSelectionHandlerSetting('auto_create') ? 'active-tags--auto-create' : '';

    // Concat element position to the Active Tags identifier.
    $tags_identifier = $items->getName();
    if (!empty($element['#field_parents'][1])) {
      $tags_identifier .= '_' . $element['#field_parents'][1];
    }

    $element += [
      '#type' => 'entity_autocomplete_active_tags',
      '#title' => $this->fieldDefinition->getLabel(),
      '#target_type' => $target_type,
      '#selection_handler' => $this->getFieldSetting('handler'),
      '#selection_settings' => $selection_settings,
      '#match_limit' => $this->getSetting('match_limit') ?? 10,
      '#min_length' => $this->getSetting('min_length') ?? 1,
      '#delimiter' => $this->getSetting('delimiter') ?? '',
      '#new_terms' => $this->getSelectionHandlerSetting('auto_create'),
      '#string_override' => $this->getSetting('string_override') ?? 0,
      '#not_exist_message' => $this->getSetting('string_override') ? $this->getSetting('not_exist_message') : NULL,
      '#not_found_message' => $this->getSetting('string_override') ? $this->getSetting('not_found_message') : NULL,
      // Entity reference field items are handling validation themselves via
      // the 'ValidReference' constraint.
      '#validate_reference' => FALSE,
      '#maxlength' => 1024,
      '#size' => 60,
      '#style' => $this->getSetting('style') ?? 'rectangle',
      '#default_value' => $referenced_entities ?? NULL,
      '#placeholder' => $this->getSetting('placeholder'),
      '#show_avatar' => $target_type == 'user' ? $this->getSetting('show_avatar') : 0,
      '#show_entity_id' => $this->getSetting('show_entity_id'),
      '#convert_uppercase' => $this->getSetting('convert_uppercase') ?? 0,
      '#attributes' => [
        'class' => [$limited, $auto_create, $tags_identifier],
      ],
      '#identifier' => $tags_identifier,
      '#cardinality' => $items->getFieldDefinition()
        ->getFieldStorageDefinition()
        ->getCardinality(),
    ];

    if ($bundle = $this->getAutocreateBundle()) {
      $element['#autocreate'] = [
        'bundle' => $bundle,
        'uid' => ($entity instanceof EntityOwnerInterface) ? $entity->getOwnerId() : \Drupal::currentUser()->id(),
      ];
    }

    if ($this->getSetting('show_info_label')) {
      $element['#info_label'] = $this->getSetting('info_label');
    }

    // Add description if it doesn't exist.
    if ($target_type) {
      $entity_definition = $this->entityTypeManager->getDefinition($target_type);
      $message = $this->t("Drag to re-order @entity_types.", ['@entity_types' => $entity_definition->getPluralLabel()]);

      if ($cardinality) {
        $element['#description'] = !empty($element['#description'])
          ? [
            '#theme' => 'item_list',
            '#items' => [
              $element['#description'],
              $message,
            ],
          ]
          : $message;
      }
    }

    $element['target_id']['#tags'] = TRUE;
    $element['target_id']['#default_value'] = $items->referencedEntities();

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues($values, array $form, FormStateInterface $form_state) {
    if (!is_string($values)) {
      return [];
    }

    $target_type = $this->getFieldSetting('target_type');
    $target_bundles = $this->getSelectionHandlerSetting('target_bundles');
    $selection_settings = $this->getFieldSetting('handler_settings') + [
      'match_operator' => $this->getSetting('match_operator'),
      'match_limit' => $this->getSetting('match_limit'),
      'min_length' => $this->getSetting('min_length'),
      'target_type' => $target_type,
      'placeholder' => $this->getSetting('placeholder'),
      'show_entity_id' => $this->getSetting('show_entity_id'),
    ];

    if ($this->getSetting('show_info_label')) {
      $selection_settings['info_label'] = $this->getSetting('info_label');
    }

    $uid = $this->currentUser->id();
    $handler = $this->selectionManager->getInstance($selection_settings);

    $data = json_decode($values, TRUE);
    if (!is_array($data)) {
      return [];
    }

    $items = [];
    $entity_storage = $this->entityTypeManager->getStorage($target_type);
    $entity_type = $entity_storage->getEntityType();

    // Get the label and bundle keys depending on the entity type.
    // E.g. node has `title` and `type`, taxonomy has `name` and `vid`.
    $label_key = $entity_type->getKey('label');
    $bundle_key = $entity_type->getKey('bundle');

    foreach ($data as $current) {
      // If an entity ID is already provided, we can just use that directly.
      if (isset($current['entity_id'])) {
        $items[] = ['target_id' => $current['entity_id']];
        continue;
      }

      // Find if a tag already exists.
      if ($label_key || $bundle_key) {
        $query = $entity_storage->getQuery()
          ->accessCheck(FALSE)
          ->condition($label_key, $current['value']);

        if ($label_key) {
          $query->condition($label_key, $current['value']);
        }
        if ($bundle_key) {
          $query->condition($bundle_key, $target_bundles, 'IN');
        }

        $ids = $query->execute();
        if ($ids !== []) {
          $items[] = ['target_id' => reset($ids)];
          continue;
        }
      }

      // Auto-create the entity if possible.
      $auto_create_bundle = $this->getAutocreateBundle();
      if ($auto_create_bundle) {
        $entity = $handler->createNewEntity($target_type, $auto_create_bundle, $current['value'], $uid);
        $items[] = ['entity' => $entity];
      }
    }

    return $items;
  }

  /**
   * Returns the options for the tag style.
   *
   * @return array
   *   List of tag style options.
   */
  protected function getTagStyleOptions() {
    return [
      'rectangle' => $this->t('Rectangular'),
      'bubble' => $this->t('Bubble style'),
    ];
  }

}
