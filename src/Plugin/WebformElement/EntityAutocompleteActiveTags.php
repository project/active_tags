<?php

namespace Drupal\active_tags\Plugin\WebformElement;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\webform\Plugin\WebformElementBase;
use Drupal\webform\Plugin\WebformElementEntityReferenceInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'entity_autocomplete_active_tags' element.
 *
 * @WebformElement(
 *   id = "entity_autocomplete_active_tags",
 *   api = "https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Entity!Element!EntityAutocomplete.php/class/EntityAutocomplete",
 *   label = @Translation("Entity autocomplete (Active Tags)"),
 *   description = @Translation("Provides a form element to select an entity reference using an active tags autocompletion."),
 *   category = @Translation("Entity reference elements"),
 * )
 */
class EntityAutocompleteActiveTags extends WebformElementBase implements WebformElementEntityReferenceInterface {

  use WebformEntityReferenceActiveTagsTrait;

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return [
      // Entity reference settings.
      'autocomplete_route_name' => 'active_tags.entity_autocomplete',
      'match_operator' => 'CONTAINS',
      'size' => 60,
      'target_type' => '',
      'selection_handler' => 'default',
      'selection_settings' => [],
      'tags' => FALSE,
      'style' => 'rectangle',
      'match_limit' => 10,
      'min_length' => 1,
      'delimiter' => '',
      'placeholder' => '',
      'show_avatar' => 1,
      'show_entity_id' => 0,
      'show_info_label' => 0,
      'info_label' => '',
      'not_exist_message' => NULL,
      'not_found_message' => NULL,
      'limit_tag_message' => "Allowed number of values limited to <strong>@cardinality</strong>.",
    ] + parent::defineDefaultProperties()
      + $this->defineDefaultMultipleProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultValue(array &$element) {
    // Make sure tags or multiple is used.
    if (!empty($element['#tags']) && isset($element['#multiple'])) {
      unset($element['#multiple']);
    }

    if (!empty($element['#default_value'])) {
      $target_type = $this->getTargetType($element);
      $entity_storage = $this->entityTypeManager->getStorage($target_type);
      if ($entities = $entity_storage->loadMultiple((array) $element['#default_value'])) {
        $element['#default_value'] = $entities;
      }
      else {
        $element['#default_value'] = [];
      }
    }
    else {
      $element['#default_value'] = [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function supportsMultipleValues() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function hasMultipleWrapper() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function hasMultipleValues(array $element) {
    if ($this->hasProperty('tags') && isset($element['#tags'])) {
      return $element['#tags'];
    }
    else {
      return parent::hasMultipleValues($element);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepare($element, $webform_submission);

    // Remove the maxlength attribute if it exists.
    $element['#maxlength'] = NULL;

    // Add the active-tags--auto-create class if auto_create is enabled.
    if (!empty($element['#selection_settings']['auto_create'])) {
      $element['#attributes']['class'][] = 'active-tags--auto-create';
    }

    // Set the cardinality based on the multiple property.
    if (isset($element['#multiple'])) {
      $element['#cardinality'] = is_int($element['#multiple']) ? $element['#multiple'] : '-1';
    }
    else {
      // Add the active-tags--limited class if multiple is not set.
      $element['#attributes']['class'][] = 'active-tags--limited';
    }

    // Set the identifier and add it as a class if webform_key is available.
    if (!empty($element['#webform_key'])) {
      $element['#identifier'] = $element['#webform_key'];
      $element['#attributes']['class'][] = $element['#identifier'];
    }

    // Set the min_length in selection settings.
    $element['#selection_settings']['min_length'] = $element['#min_length'] ?? 1;

    // Set auto_create if auto_create or auto_create_bundle are enabled.
    // This is required for proper entity autocomplete validation.
    // @see \Drupal\Core\Entity\Element\EntityAutocomplete::validateEntityAutocomplete
    if (!empty($element['#selection_settings']['auto_create'])
        || !empty($element['#selection_settings']['auto_create_bundle'])) {
      $element['#autocreate']['bundle'] = $element['#selection_settings']['auto_create_bundle'];
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareElementValidateCallbacks(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepareElementValidateCallbacks($element, $webform_submission);
    // Using a closure to pass the service to the static method.
    $entityTypeManager = $this->entityTypeManager;
    $element['#element_validate'][] = function ($element, FormStateInterface $form_state) use ($entityTypeManager) {
      EntityAutocompleteActiveTags::validateEntityAutocomplete($element, $form_state, $entityTypeManager);
    };
  }

  /**
   * Form API callback. Converts target id to an array of entity ids.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public static function validateEntityAutocomplete(array &$element, FormStateInterface $form_state, EntityTypeManagerInterface $entityTypeManager) {
    // Retrieve the value from the form state.
    $values = $form_state->getValue($element['#parents']);
    $items = json_decode($values, TRUE);

    // Check for valid JSON and non-empty values.
    if (empty($values) || (json_last_error() !== JSON_ERROR_NONE)) {
      return;
    }

    /** @var \Drupal\webform\Plugin\WebformElementManagerInterface $element_manager */
    $element_manager = \Drupal::service('plugin.manager.webform.element');
    $element_plugin = $element_manager->getElementInstance($element);
    $entity_storage = $entityTypeManager->getStorage($element["#target_type"]);

    // Handle multiple values case.
    if ($element_plugin->hasMultipleValues($element)) {
      $entity_ids = [];
      foreach ($items as $item) {
        $entity_id = static::getEntityIdFromItem($item, $entity_storage, $element['#selection_settings']);
        if (!empty($entity_id)) {
          $entity_ids[] = (string) $entity_id;
        }
      }
      // Set the valid entity IDs in the form state.
      $form_state->setValueForElement($element, $entity_ids);
    }
    else {
      // Handle single value case.
      $entity_id = static::getEntityIdFromItem($items, $entity_storage, $element['#selection_settings']);
      if (!empty($entity_id)) {
        // Set the valid entity ID in the form state.
        $form_state->setValueForElement($element, (string) $entity_id);
      }
      else {
        // Set NULL if no valid entity ID is found.
        $form_state->setValueForElement($element, NULL);
      }
    }
  }

  /**
   * Get the entity id from the submitted and processed #value.
   *
   * @param array|string $item
   *   The entity item.
   * @param Object $entity_storage
   *   The entity storage handler.
   * @param array $settings
   *   The settings array.
   *
   * @return string
   *   The entity id.
   *
   * @throws \Exception
   *   If the vocabulary is not found.
   */
  protected static function getEntityIdFromItem($item, $entity_storage, array $settings) {
    // Check if entity_id is already set.
    if (isset($item['entity_id'])) {
      return $item['entity_id'];
    }

    // Check if the value is set and valid.
    if (isset($item['value'])) {
      if (!empty($settings['target_bundles'])) {
        foreach ($settings['target_bundles'] as $vid) {
          // Check if the term already exists.
          $terms = $entity_storage->loadByProperties(['name' => $item['value'], 'vid' => $vid]);
          if ($terms) {
            $term = reset($terms);
            return $term->id();
          }

          // If auto_create is enabled, create a new term.
          if (!empty($settings['auto_create'])) {
            $term = Term::create([
              'vid' => $vid,
              'name' => $item['value'],
            ]);
            $term->save();
            return $term->id();
          }
        }
      }
    }

    // Return empty string if no entity_id is found or created.
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getExportDefaultOptions() {
    return [
      'entity_reference_items' => ['id', 'title', 'url'],
    ];
  }

  /**
   * Returns the options for the match operator.
   *
   * @return array
   *   List of options.
   */
  protected function getMatchOperatorOptions() {
    return [
      'STARTS_WITH' => $this->t('Starts with'),
      'CONTAINS' => $this->t('Contains'),
    ];
  }

  /**
   * Returns the options for the tag style.
   *
   * @return array
   *   List of tag style options.
   */
  protected function getTagStyleOptions() {
    return [
      'rectangle' => $this->t('Rectangular'),
      'bubble' => $this->t('Bubble style'),
    ];
  }

}
