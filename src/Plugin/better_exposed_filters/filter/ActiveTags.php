<?php

namespace Drupal\active_tags\Plugin\better_exposed_filters\filter;

use Drupal\better_exposed_filters\Plugin\better_exposed_filters\filter\FilterWidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Active tags widget implementation.
 *
 * @BetterExposedFiltersFilterWidget(
 *   id = "bef_active_tags",
 *   label = @Translation("Active tags"),
 * )
 */
class ActiveTags extends FilterWidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The entity_autocomplete key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected KeyValueStoreInterface $keyValue;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->keyValue = $container->get('keyvalue')->get('entity_autocomplete');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $config = parent::defaultConfiguration();
    $config['advanced']['style'] = 'rectangle';
    $config['advanced']['match_operator'] = 'CONTAINS';
    $config['advanced']['max_items'] = 10;
    $config['advanced']['min_length'] = 1;
    $config['advanced']['show_entity_id'] = 0;
    $config['advanced']['placeholder'] = '';

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $form_state): void {
    parent::exposedFormAlter($form, $form_state);

    $field_id = $this->getExposedFilterFieldId();
    $wrapper_key = $field_id . '_wrapper';
    $base_settings = [
      '#type' => 'entity_autocomplete_active_tags',
      '#selection_handler' => 'default',
      '#selection_settings' => [],
      '#match_operator' => $this->configuration['advanced']['match_operator'],
      '#max_items' => (int) $this->configuration['advanced']['max_items'],
      '#min_length' => (int) $this->configuration['advanced']['min_length'] ?? 1,
      '#style' => $this->configuration['advanced']['style'] ?? 'rectangle',
      '#show_entity_id' => $this->configuration['advanced']['show_entity_id'],
      '#placeholder' => $this->configuration['advanced']['placeholder'],
      '#attributes' => ['class' => [$field_id]],
      '#element_validate' => [[$this, 'elementValidate']],
    ];

    $form[$wrapper_key][$field_id] = !empty($form[$wrapper_key][$field_id])
      ? array_merge($form[$wrapper_key][$field_id], $base_settings)
      : (!empty($form[$field_id])
        ? array_merge($form[$field_id], $base_settings)
        : $base_settings);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {

    $form = parent::buildConfigurationForm($form, $form_state);

    $form['advanced']['match_operator'] = [
      '#type' => 'radios',
      '#options' => $this->getMatchOperatorOptions(),
      '#title' => $this->t('Autocomplete matching'),
      '#description' => $this->t('Select the method used to collect autocomplete suggestions. Note that <em>Contains</em> can cause performance issues on sites with thousands of entities.'),
      '#default_value' => $this->configuration['advanced']['match_operator'],
    ];

    $form['advanced']['max_items'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Number of results'),
      '#description' => $this->t('The number of suggestions that will be listed. Use <em>0</em> to remove the limit.'),
      '#default_value' => $this->configuration['advanced']['max_items'],
    ];

    $form['advanced']['min_length'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Minimum length'),
      '#description' => $this->t('Set the minimum number of characters needed to trigger the suggestion list.'),
      '#default_value' => $this->configuration['advanced']['min_length'],
    ];

    $form['advanced']['style'] = [
      '#type' => 'select',
      '#options' => $this->getTagStyleOptions(),
      '#title' => $this->t('Tag style'),
      '#description' => $this->t('Choose the style for displaying tags from the available options.'),
      '#default_value' => $this->configuration['advanced']['style'],
    ];

    $form['advanced']['show_entity_id'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include entity id'),
      '#description' => $this->t('Enable this to include the entity ID within each tag.'),
      '#default_value' => $this->configuration['advanced']['show_entity_id'],
    ];

    $form['advanced']['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#description' => $this->t('Text to be shown in the active tags field until a value is selected.'),
      '#default_value' => $this->configuration['advanced']['placeholder'],
    ];

    // Unset default placeholder text option.
    unset($form['advanced']['placeholder_text']);

    return $form;
  }

  /**
   * Validates and processes the autocomplete element values.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @throws \JsonException
   */
  public static function elementValidate(array $element, FormStateInterface $form_state): void {
    $value = $form_state->getValue($element['#parents']);
    if ($value && ($items = json_decode($value, TRUE, 512, JSON_THROW_ON_ERROR))) {
      $formatted_items = self::formattedItems($items);
      if (!empty($formatted_items)) {
        $form_state->setValue($element['#parents'], $formatted_items);
      }
    }
  }

  /**
   * Formats filter items.
   *
   * @param array $items
   *   The filter items.
   *
   * @return array
   *   The formatted filter items.
   */
  protected static function formattedItems(array $items): array {
    foreach ($items as $item) {
      $formatted_items[] = [
        'target_id' => $item['entity_id'],
      ];
    }

    return $formatted_items ?? [];
  }

  /**
   * Returns the options for the match operator.
   *
   * @return array
   *   List of options.
   */
  protected function getMatchOperatorOptions() {
    return [
      'STARTS_WITH' => $this->t('Starts with'),
      'CONTAINS' => $this->t('Contains'),
    ];
  }

  /**
   * Returns the options for the match operator.
   *
   * @return array
   *   List of options.
   */
  protected function getTagStyleOptions() {
    return [
      'rectangle' => $this->t('Rectangular'),
      'bubble' => $this->t('Bubble style'),
    ];
  }

}
