<?php

namespace Drupal\active_tags\Plugin\facets\widget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\Widget\WidgetPluginBase;

/**
 * The Facets Active Tags widget.
 *
 * @FacetsWidget(
 *   id = "active_tags",
 *   label = @Translation("Active tags"),
 *   description = @Translation("A configurable widget that shows an active tags component."),
 * )
 */
class ActiveTagsWidget extends WidgetPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'style' => 'rectangle',
      'match_operator' => 'CONTAINS',
      'max_items' => 10,
      'min_length' => 0,
      'show_entity_id' => 0,
      'placeholder' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet) {
    $form = parent::buildConfigurationForm($form, $form_state, $facet);
    $form['show_entity_id'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include entity id'),
      '#description' => $this->t('Enable this to include the entity ID within each tag.'),
      '#default_value' => $this->getConfiguration()['show_entity_id'],
    ];
    $form['style'] = [
      '#type' => 'select',
      '#options' => $this->getTagStyleOptions(),
      '#title' => $this->t('Tag style'),
      '#description' => $this->t('Choose the style for displaying tags from the available options.'),
      '#default_value' => $this->getConfiguration()['style'],
    ];
    $form['match_operator'] = [
      '#type' => 'radios',
      '#title' => $this->t('Autocomplete matching'),
      '#default_value' => $this->getConfiguration()['match_operator'],
      '#options' => $this->getMatchOperatorOptions(),
      '#description' => $this->t('Select the method used to collect autocomplete suggestions. Note that <em>Contains</em> can cause performance issues on sites with thousands of entities.'),
      '#states' => [
        'visible' => [
          ':input[name$="widget_config[autocomplete]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['max_items'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of results'),
      '#default_value' => $this->getConfiguration()['max_items'],
      '#min' => 0,
      '#description' => $this->t('The number of suggestions that will be listed. Use <em>0</em> to remove the limit.'),
    ];
    $form['min_length'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Minimum length'),
      '#description' => $this->t('Set the minimum number of characters needed to trigger the suggestion list.'),
      '#default_value' => $this->getConfiguration()['min_length'],
    ];
    $form['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#default_value' => $this->getConfiguration()['placeholder'],
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet) {
    $build = parent::build($facet);
    $this->appendWidgetLibrary($build);
    return $build;
  }

  /**
   * Appends widget library and relevant information for it to build array.
   *
   * @param array $build
   *   Reference to build array.
   */
  protected function appendWidgetLibrary(array &$build) {
    $build['#attributes']['class'][] = 'active-tags-facets-widget';
    $build['#attributes']['class'][] = 'js-facets-active-tags';
    $build['#attributes']['class'][] = 'hidden';
    $build['#attached']['library'][] = 'active_tags/assets.facets';
    $build['#attached']['drupalSettings']['activeTags']['facets_widget']['match_operator'] = $this->getConfiguration()['match_operator'];
    $build['#attached']['drupalSettings']['activeTags']['facets_widget']['placeholder'] = $this->getConfiguration()['placeholder'];
    $build['#attached']['drupalSettings']['activeTags']['facets_widget']['max_items'] = $this->getConfiguration()['max_items'];
    $build['#attached']['drupalSettings']['activeTags']['facets_widget']['min_length'] = $this->getConfiguration()['min_length'];
    $build['#attached']['drupalSettings']['activeTags']['facets_widget']['style'] = $this->getConfiguration()['style'];
    $build['#attached']['drupalSettings']['activeTags']['facets_widget']['show_entity_id'] = $this->getConfiguration()['show_entity_id'];
  }

  /**
   * Returns the options for the match operator.
   *
   * @return array
   *   List of options.
   */
  protected function getMatchOperatorOptions(): array {
    return [
      'STARTS_WITH' => $this->t('Starts with'),
      'CONTAINS' => $this->t('Contains'),
    ];
  }

  /**
   * Returns the options for the match operator.
   *
   * @return array
   *   List of options.
   */
  protected function getTagStyleOptions(): array {
    return [
      'rectangle' => $this->t('Rectangular'),
      'bubble' => $this->t('Bubble style'),
    ];
  }

}
